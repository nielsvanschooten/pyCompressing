﻿import sys, os
import json


class Compressor:
	"""Compress a file with a binary commression"""
	
	def __init__(self, path=os.path.abspath(__file__), filename=__file__, backdir=0):
		self.path = self.stripPath(path, backdir)
		self.fileName = os.path.basename(filename)
		self.binary = []
		self.fileData = []
		print(self.path)
		self.getFileData()

	def getFileData(self):
		with open(self.path + self.fileName, 'rb') as file:
			for line in file.readlines():
				print(line)
				self.fileData.append(line)


	def stripPath(self, path, amountBack=0):
		pathsplit = path.split("\\")
		newPath = ""
		for pathElement in range((len(pathsplit)-1)-amountBack):
			newPath += pathsplit[pathElement] + "/"

		return newPath

	def CompressFile(self, filename=None):
		if filename == None:
			filename = self.fileName + ".pyzip"

		lineLength = 0
		lineIndex = 0
		dictCol = {}

		for byteline in self.fileData:
			for byte in range(len(byteline)):
				try:
					dictCol[byteline[byte]].append([byte, lineIndex])
				except:
					dictCol[byteline[byte]] = [[byte, lineIndex]]
			lineIndex += 1


		print("File is Compressed")
		json.dump(dictCol, open(filename, "w"))
		print("Written compressed file to disk")



	def DecompressFile(self, filename=None, decfile =None):
		if filename == None:
			filename = self.fileName + ".dec"

		if decfile == None:
			decfile = self.fileName + ".pyzip"

		linelength